package com.example.agung.footballapp.presenter.match

import com.example.agung.footballapp.model.match.EventsItem
import com.example.agung.footballapp.model.match.Match
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.match.MatchView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// This Presenter class belongs to PlaceholderFragment inside MatchFragment
class MatchPresenter(private val view: MatchView, private val apiClass: ApiClass) {
    // Get data for RecyclerView
    fun getData(position: Int?, leagueId: String) {
        var call: Call<Match>? = null
        when (position) {
            0 -> call = apiClass.getApiInterface()?.getLastMatch(leagueId)
            1 -> call = apiClass.getApiInterface()?.getNextMatch(leagueId)
        }

        val callback = object : Callback<Match> {
            override fun onFailure(call: Call<Match>, t: Throwable) {}

            override fun onResponse(call: Call<Match>, response: Response<Match>) {
                if (response.isSuccessful) {
                    val match = response.body()!!
                    val list = match.events as MutableList<EventsItem>

                    view.showData(list)
                }
            }
        }

        call?.enqueue(callback)
    }
}