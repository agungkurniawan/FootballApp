package com.example.agung.footballapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FavoriteTeam(
    val id: Int? = null,
    val teamName: String? = null,
    val foundedYear: Int? = null,
    val stadion: String? = null,
    val description: String? = null,
    val teamLogo: String? = null
) : Parcelable {
    companion object {
        const val TABLE_FAVORITE = "FAVORITE_TEAM"
        const val ID = "ID_TEAM"
        const val TEAM_NAME = "TEAM_NAME"
        const val FOUNDED_YEAR = "FOUNDED_YEAR"
        const val STADION = "STADIUM"
        const val DESCRIPTION = "DESCRIPTION"
        const val TEAM_LOGO = "LOGO"
    }
}