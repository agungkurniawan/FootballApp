package com.example.agung.footballapp.activity.favorite

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.adapter.FavoriteMatchAdapter
import com.example.agung.footballapp.adapter.FavoriteTeamAdapter
import com.example.agung.footballapp.model.FavoriteMatch
import com.example.agung.footballapp.model.FavoriteTeam
import com.example.agung.footballapp.presenter.favorite.FavoriteListPresenter
import com.example.agung.footballapp.view.favorite.FavoriteListView
import kotlinx.android.synthetic.main.fragment_favorite_list.*

class FavoriteListFragment : Fragment(), FavoriteListView {
    private lateinit var presenter: FavoriteListPresenter

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"

        fun newInstance(position: Int): FavoriteListFragment {
            val fragment = FavoriteListFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, position)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = FavoriteListPresenter(context, this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe_favorite.setColorSchemeColors(ContextCompat.getColor(context!!, R.color.colorPrimary))
        swipe_favorite.isRefreshing = true
        swipe_favorite.setOnRefreshListener {
            presenter.getMatchList(arguments?.getInt(ARG_SECTION_NUMBER)!!)
        }
    }

    override fun onResume() {
        super.onResume()

        presenter.getMatchList(arguments?.getInt(ARG_SECTION_NUMBER)!!)
    }

    override fun showSnackbar(message: String) {
        Snackbar.make(rv_favorite_match, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun showDataMatch(items: List<FavoriteMatch>) {
        hideLoading()

        val adapter = FavoriteMatchAdapter(context, items)
        rv_favorite_match.hasFixedSize()
        rv_favorite_match.layoutManager = LinearLayoutManager(context)
        rv_favorite_match.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun showDataTeam(items: List<FavoriteTeam>) {
        hideLoading()
        val adapter = FavoriteTeamAdapter(context!!, items)
        rv_favorite_match.hasFixedSize()
        rv_favorite_match.layoutManager = LinearLayoutManager(context)
        rv_favorite_match.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun hideLoading() {
        swipe_favorite.isRefreshing = false
        rv_favorite_match.visibility = View.VISIBLE
    }

    override fun hideRecyclerView() {
        rv_favorite_match.visibility = View.GONE
    }
}
