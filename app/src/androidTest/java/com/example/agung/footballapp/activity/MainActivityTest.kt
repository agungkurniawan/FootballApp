package com.example.agung.footballapp.activity


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.swipeLeft
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.result
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest() {
        // Select Match 1
//        val recyclerView = onView(
//            allOf(
//                withId(R.id.rv_main),
//                childAtPosition(
//                    withId(R.id.swipe_main),
//                    0
//                )
//            )
//        )
//        Thread.sleep(3000)
//        recyclerView.perform(actionOnItemAtPosition<ViewHolder>(1, click()))
        val recyclerView = onView(
            result(withId(R.id.rv_main), 0)
        )
        Thread.sleep(3000)
        recyclerView.perform(actionOnItemAtPosition<ViewHolder>(1, click()))

        // Add to favorite Match 1
        val actionMenuItemView = onView(
            allOf(
                withId(R.id.add_to_favorite), withContentDescription("Add to favorite"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.action_bar),
                        2
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        Thread.sleep(1000)
        actionMenuItemView.perform(click())

        // Back to Match
        pressBack()

        // Select Match 2
        val recyclerView2 = onView(
            result(
                withId(R.id.rv_main), 0
            )
        )
        recyclerView2.perform(actionOnItemAtPosition<ViewHolder>(3, click()))

        // Add to favorite Match 2
        val actionMenuItemView2 = onView(
            allOf(
                withId(R.id.add_to_favorite), withContentDescription("Add to favorite"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.action_bar),
                        2
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        Thread.sleep(1000)
        actionMenuItemView2.perform(click())

        pressBack()

        // Select Team Fragment
        val bottomNavigationItemView = onView(
            allOf(
                withId(R.id.navigation_teams), withContentDescription("Teams"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.navigation),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        bottomNavigationItemView.perform(click())

        // Select Team 1
        val recyclerView3 = onView(
            result(
                withId(R.id.rv_teams), 0
            )
        )
        Thread.sleep(3000)
        recyclerView3.perform(actionOnItemAtPosition<ViewHolder>(0, click()))

        // Add to favorite Team 1
        val actionMenuItemView3 = onView(
            allOf(
                withId(R.id.add_to_favorite), withContentDescription("Add to favorite"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.toolbar),
                        3
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        actionMenuItemView3.perform(click())

        pressBack()

        // Select Team 2
        val recyclerView4 = onView(
            result(
                withId(R.id.rv_teams),
                0
            )
        )
        Thread.sleep(3000)
        recyclerView4.perform(actionOnItemAtPosition<ViewHolder>(1, click()))

        // Add to favorite Team 2
        val actionMenuItemView4 = onView(
            allOf(
                withId(R.id.add_to_favorite), withContentDescription("Add to favorite"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.toolbar),
                        3
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        actionMenuItemView4.perform(click())

        pressBack()

        // View match favorite
        val bottomNavigationItemView2 = onView(
            allOf(
                withId(R.id.navigation_favorites), withContentDescription("Favorites"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.navigation),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        bottomNavigationItemView2.perform(click())

        // View team favorite
        val viewPager3 = onView(
            allOf(
                withId(R.id.vp_container),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.fragment_placeholder),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        viewPager3.perform(swipeLeft())
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
