package com.example.agung.footballapp.presenter.favorite

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import android.widget.Toast
import com.example.agung.footballapp.database.database
import com.example.agung.footballapp.model.FavoriteMatch
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class FavoriteMatchDetailsPresenter(val context: Context?) {
    fun addToFavorites(items: FavoriteMatch) {
        try {
            context?.database?.use {
                insert(FavoriteMatch.TABLE_FAVORITE,
                    FavoriteMatch.ID to items.id,
                    FavoriteMatch.MATCH_DATE to items.matchDate,
                    FavoriteMatch.MATCH_TIME to items.matchTime,
                    FavoriteMatch.NAME_HOME to items.nameHomeTeam,
                    FavoriteMatch.NAME_AWAY to items.nameAwayTeam,
                    FavoriteMatch.LOGO_HOME to items.logoHomeTeam,
                    FavoriteMatch.LOGO_AWAY to items.logoAwayTeam,
                    FavoriteMatch.SCORE_HOME to items.scoreHomeTeam,
                    FavoriteMatch.SCORE_AWAY to items.scoreAwayTeam,
                    FavoriteMatch.GOALS_HOME to items.goalsHomeTeam,
                    FavoriteMatch.GOALS_AWAY to items.goalsAwayTeam,
                    FavoriteMatch.SHOTS_HOME to items.shotsHomeTeam,
                    FavoriteMatch.SHOTS_AWAY to items.shotsAwayTeam,
                    FavoriteMatch.GOALKEEPER_HOME to items.goalkeeperHomeTeam,
                    FavoriteMatch.GOALKEEPER_AWAY to items.goalkeeperAwayTeam,
                    FavoriteMatch.DEFENSE_HOME to items.defenseHomeTeam,
                    FavoriteMatch.DEFENSE_AWAY to items.defenseAwayTeam,
                    FavoriteMatch.MIDFIELD_HOME to items.midfieldHomeTeam,
                    FavoriteMatch.MIDFIELD_AWAY to items.midfieldAwayTeam,
                    FavoriteMatch.FORWARD_HOME to items.forwardHomeTeam,
                    FavoriteMatch.FORWARD_AWAY to items.forwardAwayTeam)
            }
            Toast.makeText(context, "Added to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Log.e(FavoriteMatchDetailsPresenter::class.java.simpleName, e.localizedMessage)
        }
    }

    fun isFavorite(idMatch: Int?): Boolean {
        var isNotEmpty = false

        context?.database?.use {
            val result = select(FavoriteMatch.TABLE_FAVORITE)
                .whereArgs("${FavoriteMatch.ID} = {id}", "id" to idMatch.toString())
                .parseList(classParser<FavoriteMatch>())
            isNotEmpty = result.isNotEmpty()
        }

        return isNotEmpty
    }

    fun removeFromFavorite(idMatch: Int?) {
        try {
            context?.database?.use {
                delete(FavoriteMatch.TABLE_FAVORITE,
                    "${FavoriteMatch.ID} = {id}",
                    "id" to idMatch.toString())
            }

            Toast.makeText(context, "Removed from favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Log.e(FavoriteMatchDetailsPresenter::class.java.simpleName, e.localizedMessage)
        }
    }
}