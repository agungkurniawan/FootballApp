package com.example.agung.footballapp.activity.team


import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.adapter.PlayerAdapter
import com.example.agung.footballapp.model.player.PlayerItem
import com.example.agung.footballapp.presenter.team.PlayerPresenter
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.team.PlayerView
import kotlinx.android.synthetic.main.fragment_player.*
import kotlinx.android.synthetic.main.fragment_player.view.*

class PlayerFragment : Fragment(), PlayerView {
    private lateinit var v: View

    private lateinit var apiClass: ApiClass
    private lateinit var presenter: PlayerPresenter

    companion object {
        private const val DESCRIPTION = "club_description"

        fun newInstance(clubId: String): PlayerFragment {
            val fragment = PlayerFragment()
            val args = Bundle()
            args.putString(DESCRIPTION, clubId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        apiClass = ApiClass()
        presenter = PlayerPresenter(this, apiClass, arguments?.getString(DESCRIPTION).toString())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_player, container, false)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.getPlayer()
    }

    override fun showLoading() {
        v.pb_player.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        v.pb_player.visibility = View.GONE
    }

    override fun showData(items: List<PlayerItem>) {
        val adapter = PlayerAdapter(context!!, items)
        v.rv_player.adapter = adapter
        v.rv_player.hasFixedSize()
        v.rv_player.layoutManager = LinearLayoutManager(context)
        adapter.notifyDataSetChanged()
    }

    override fun showSnackbar(message: String) {
        Snackbar.make(rv_player, message, Snackbar.LENGTH_SHORT).show()
    }
}
