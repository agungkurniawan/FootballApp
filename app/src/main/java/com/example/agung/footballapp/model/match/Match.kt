package com.example.agung.footballapp.model.match

import com.google.gson.annotations.SerializedName

data class Match(
    @field:SerializedName("events")
    val events: List<EventsItem?>? = null
)