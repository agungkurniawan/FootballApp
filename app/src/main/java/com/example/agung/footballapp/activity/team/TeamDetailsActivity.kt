package com.example.agung.footballapp.activity.team

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.agung.footballapp.R
import com.example.agung.footballapp.model.teams.TeamsItem
import com.example.agung.footballapp.presenter.team.TeamDetailsPresenter
import com.example.agung.footballapp.view.team.TeamDetailsView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_team_details.*

class TeamDetailsActivity : AppCompatActivity(), TeamDetailsView {
    private lateinit var items: TeamsItem
    private lateinit var presenter: TeamDetailsPresenter

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        vp_container.adapter = mSectionsPagerAdapter

        vp_container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(vp_container))

        if (intent?.extras?.containsKey("team_data")!!) {
            items = intent.getParcelableExtra("team_data")

            toolbarSettings()

            Picasso.get().load(items.strTeamBadge).into(iv_logo_team)
            tv_team_name.text = items.strTeam
            tv_year.text = items.intFormedYear
            tv_stadion.text = items.strStadium
        }

        presenter = TeamDetailsPresenter(this, this)
    }

    private fun toolbarSettings() {
        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { p0, p1 ->
            var isShow = true
            var scrollRange = -1

            if (scrollRange == -1) {
                scrollRange = p0?.totalScrollRange!!
            }

            if (scrollRange + p1 == 0) {
                toolbar_layout.title = items.strTeam
                isShow = true
            } else if (isShow) {
                toolbar_layout.title = " "
                isShow = false
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)

        if (presenter.isFavorite(items.idTeam.toString())) {
            menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_star_black_24dp)
        } else {
            menu?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_star_border_black_24dp)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                onBackPressed()
                true
            }
            R.id.add_to_favorite -> {
                if (presenter.isFavorite(items.idTeam.toString())) {
                    presenter.removeFromFavorite(items.idTeam.toString())
                    item.icon = ContextCompat.getDrawable(this, R.drawable.ic_star_border_black_24dp)
                } else {
                    presenter.addToFavorite(items)
                    item.icon = ContextCompat.getDrawable(this, R.drawable.ic_star_black_24dp)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showSnackbar(message: String) {
        Snackbar.make(toolbar, message, Snackbar.LENGTH_SHORT).show()
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(p0: Int): Fragment {
            return if (p0 == 0) {
                OverviewFragment.newInstance(items.strDescriptionEN.toString())
            } else {
                PlayerFragment.newInstance(items.idTeam.toString())
            }
        }

        override fun getCount(): Int {
            return 2
        }
    }
}
