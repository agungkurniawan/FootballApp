package com.example.agung.footballapp.view.team

import com.example.agung.footballapp.model.player.PlayerItem

interface PlayerView {
    fun showLoading()
    fun hideLoading()
    fun showData(items: List<PlayerItem>)
    fun showSnackbar(message: String)
}