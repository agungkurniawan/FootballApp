package com.example.agung.footballapp.presenter.team

import com.example.agung.footballapp.model.teams.TeamList
import com.example.agung.footballapp.model.teams.TeamsItem
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.team.TeamView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TeamPresenter(
    private val view: TeamView,
    private val apiClass: ApiClass) {

    fun getTeamList(idLeague: String) {
        val teamRequest = apiClass.getApiInterface()?.getTeamByLeague(idLeague)
        teamRequest?.enqueue(object : Callback<TeamList> {
            override fun onFailure(call: Call<TeamList>, t: Throwable) {}

            override fun onResponse(call: Call<TeamList>, response: Response<TeamList>) {
                if (response.isSuccessful) {
                    val teams = response.body()!!
                    val list = teams.teams as MutableList<TeamsItem>

                    view.showData(list)
                }
            }
        })
    }

}