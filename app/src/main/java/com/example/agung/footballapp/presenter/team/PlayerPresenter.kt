package com.example.agung.footballapp.presenter.team

import com.example.agung.footballapp.model.player.PlayerItem
import com.example.agung.footballapp.model.player.Player
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.team.PlayerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PlayerPresenter(private val view: PlayerView, private val apiClass: ApiClass, private val idClub: String) {
    fun getPlayer() {
        view.showLoading()

        val call = apiClass.getApiInterface()?.getPlayerByClub(idClub)
        call?.enqueue(object : Callback<Player> {
            override fun onFailure(call: Call<Player>, t: Throwable) {}

            override fun onResponse(call: Call<Player>, response: Response<Player>) {
                if (response.isSuccessful) {
                    val players = response.body()!!
                    val list = players.player as MutableList<PlayerItem>

                    view.showData(list)
                }
            }
        })

        view.hideLoading()
    }
}