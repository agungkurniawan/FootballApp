package com.example.agung.footballapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.team.TeamDetailsActivity
import com.example.agung.footballapp.model.teams.TeamsItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_team.view.*

class SearchTeamAdapter(private val context: Context, private val list: List<TeamsItem>) :
    RecyclerView.Adapter<SearchTeamAdapter.SearchTeamViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SearchTeamViewHolder {
        return SearchTeamViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.list_team,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(p0: SearchTeamViewHolder, p1: Int) {
        p0.bindItem(list[p1])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class SearchTeamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(teamsItem: TeamsItem) {
            Picasso.get().load(teamsItem.strTeamBadge).into(itemView.iv_logo_team)
            itemView.tv_team_name.text = teamsItem.strTeam

            itemView.setOnClickListener {
                val i = Intent(context, TeamDetailsActivity::class.java)
                i.putExtra("team_data", teamsItem)
                context.startActivity(i)
            }
        }
    }
}