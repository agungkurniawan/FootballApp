package com.example.agung.footballapp.presenter

import android.util.Log
import com.example.agung.footballapp.model.match.EventsItem
import com.example.agung.footballapp.model.search.SearchResults
import com.example.agung.footballapp.model.teams.TeamList
import com.example.agung.footballapp.model.teams.TeamsItem
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.SearchMatchTeamView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchPresenter(private val view: SearchMatchTeamView, private val apiClass: ApiClass) {
    fun getSearchResult(keyword: String?) {
        view.showLoading()

        val call = apiClass.getApiInterface()?.getSearchResults(keyword.toString())
        call?.enqueue(object : Callback<SearchResults> {
            override fun onFailure(call: Call<SearchResults>, t: Throwable) {}

            override fun onResponse(call: Call<SearchResults>, response: Response<SearchResults>) {
                if (response.isSuccessful) {
                    try {
                        val searchBody = response.body()!!
                        val results = searchBody.event
                        Log.d(SearchPresenter::class.java.simpleName, "Search results: $results")
                        view.showData(results as MutableList<EventsItem>)
                    } catch (e: TypeCastException) {
                        view.showToast("Can't find what you're looking for")
                        view.hideLoading()
                    }
                }
            }
        })
    }

    fun getTeamSearch(teamName: String) {
        view.showLoading()
        val call = apiClass.getApiInterface()?.getTeamBySearch(teamName)
        call?.enqueue(object : Callback<TeamList> {
            override fun onFailure(call: Call<TeamList>, t: Throwable) {}

            override fun onResponse(call: Call<TeamList>, response: Response<TeamList>) {
                if (response.isSuccessful) {
                    val result = response.body()!!
                    val list = result.teams as MutableList<TeamsItem>

                    view.showTeam(list)
                }
            }
        })
    }
}