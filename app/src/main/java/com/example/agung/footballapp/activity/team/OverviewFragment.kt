package com.example.agung.footballapp.activity.team

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import kotlinx.android.synthetic.main.fragment_overview.view.*

class OverviewFragment : Fragment() {
    private lateinit var v: View

    companion object {
        private const val DESCRIPTION = "club_description"

        fun newInstance(description: String): OverviewFragment {
            val fragment = OverviewFragment()
            val args = Bundle()
            args.putString(DESCRIPTION, description)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_overview, container, false)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        v.tv_description.text = arguments?.getString(DESCRIPTION)
    }


}
