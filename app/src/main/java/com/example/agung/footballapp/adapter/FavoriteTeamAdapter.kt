package com.example.agung.footballapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.favorite.FavoriteTeamDetailsActivity
import com.example.agung.footballapp.model.FavoriteTeam
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_team.view.*

class FavoriteTeamAdapter(private val context: Context?, private val list: List<FavoriteTeam>)
    : RecyclerView.Adapter<FavoriteTeamAdapter.FavoriteTeamViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FavoriteTeamViewHolder {
        return FavoriteTeamViewHolder(LayoutInflater.from(context).inflate(R.layout.list_team, p0, false))
    }

    override fun onBindViewHolder(p0: FavoriteTeamViewHolder, p1: Int) {
        p0.bindItem(list[p1])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class FavoriteTeamViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bindItem(items: FavoriteTeam) {
            Picasso.get().load(items.teamLogo).into(itemView.iv_logo_team)
            itemView.tv_team_name.text = items.teamName

            itemView.setOnClickListener {
                val i = Intent(context, FavoriteTeamDetailsActivity::class.java)
                i.putExtra("team_data", items)
                context?.startActivity(i)
            }
        }
    }
}