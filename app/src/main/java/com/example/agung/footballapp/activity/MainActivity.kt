package com.example.agung.footballapp.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.favorite.FavoriteFragment
import com.example.agung.footballapp.activity.match.MatchFragment
import com.example.agung.footballapp.activity.team.TeamFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initFragment(savedInstanceState,
            MatchFragment(), MatchFragment::class.java.simpleName)

        navigation.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_match -> {
                    initFragment(savedInstanceState,
                        MatchFragment(), MatchFragment::class.java.simpleName)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_teams -> {
                    initFragment(savedInstanceState,
                        TeamFragment(), TeamFragment::class.java.simpleName)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_favorites -> {
                    initFragment(savedInstanceState,
                        FavoriteFragment(), FavoriteFragment::class.java.simpleName)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        })
    }

    private fun initFragment(savedInstanceState: Bundle?, fragment: Fragment, classSimpleName: String) {
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_placeholder, fragment, classSimpleName).commit()
        }
    }
}
