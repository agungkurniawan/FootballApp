package com.example.agung.footballapp.view.team

interface TeamDetailsView {
    fun showSnackbar(message: String)
}