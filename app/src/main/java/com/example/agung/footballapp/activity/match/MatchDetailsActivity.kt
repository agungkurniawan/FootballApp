package com.example.agung.footballapp.activity.match

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.agung.footballapp.R
import com.example.agung.footballapp.changeDateFormat
import com.example.agung.footballapp.changeTime
import com.example.agung.footballapp.model.match.EventsItem
import com.example.agung.footballapp.presenter.match.MatchDetailsPresenter
import com.example.agung.footballapp.view.match.MatchDetailsView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_match_details.*

class MatchDetailsActivity : AppCompatActivity(), MatchDetailsView {
    private lateinit var presenter: MatchDetailsPresenter
    private lateinit var matchInfo: EventsItem

    private var homeLogo: String? = null
    private var awayLogo: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_details)

        if (intent?.extras?.containsKey("data")!!) {
            matchInfo = intent.getParcelableExtra("data")

            supportActionBar?.title = matchInfo.strEvent

            presenter = MatchDetailsPresenter(this, this)

            presenter.getLogo(matchInfo.idHomeTeam, "home")
            presenter.getLogo(matchInfo.idAwayTeam, "away")

            displayInfo(matchInfo)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)

        if (presenter.isFavorite(matchInfo.idEvent)) {
            menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_star_black_24dp)
        } else {
            menu?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_star_border_black_24dp)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                onBackPressed()
                true
            }
            R.id.add_to_favorite -> {
                if (homeLogo.isNullOrEmpty() || awayLogo.isNullOrEmpty()) {
                    Toast.makeText(this, "Please wait while loading the logo", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    if (presenter.isFavorite(matchInfo.idEvent)) {
                        presenter.removeFromFavorite(matchInfo.idEvent)
                        item.setIcon(R.drawable.ic_star_border_black_24dp)
                    } else {
                        presenter.addToFavorite(matchInfo, homeLogo.toString(), awayLogo.toString())
                        item.setIcon(R.drawable.ic_star_black_24dp)
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun displayInfo(items: EventsItem) {
        tv_date.text = changeDateFormat(items.dateEvent.toString())
        tv_time.text = changeTime(items.dateEvent.toString(), items.strTime.toString())
        if (items.intHomeScore == null && items.intAwayScore == null) {
            tv_score_home.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
            tv_score_away.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        } else {
            tv_score_home.text = items.intHomeScore
            tv_score_away.text = items.intAwayScore
        }
        tv_home_team.text = items.strHomeTeam
        tv_away_team.text = items.strAwayTeam
        tv_home_goals.text = items.strHomeGoalDetails?.replace(";", "\n")
        tv_away_goals.text = items.strAwayGoalDetails?.replace(";", "\n")
        tv_home_shots.text = items.intHomeShots
        tv_away_shots.text = items.intAwayShots
        tv_home_goalkeeper.text = items.strHomeLineupGoalkeeper?.replace(";", "")
        tv_away_goalkeeper.text = items.strAwayLineupGoalkeeper?.replace(";", "")
        tv_home_defense.text = items.strHomeLineupDefense?.replace("; ", "\n")
        tv_away_defense.text = items.strAwayLineupDefense?.replace("; ", "\n")
        tv_home_midfield.text = items.strHomeLineupMidfield?.replace("; ", "\n")
        tv_away_midfield.text = items.strAwayLineupMidfield?.replace("; ", "\n")
        tv_home_forward.text = items.strHomeLineupForward?.replace("; ", "\n")
        tv_away_forward.text = items.strAwayLineupForward?.replace("; ", "\n")
    }

    override fun displayClubLogo(logoUrl: String?, type: String?) {
        if (type.equals("home")) {
            homeLogo = logoUrl
            Picasso.get().load(logoUrl).into(iv_home_team)
        } else {
            awayLogo = logoUrl
            Picasso.get().load(logoUrl).into(iv_away_team)
        }
    }

    override fun showSnackbar(message: String) {
        Snackbar.make(details_layout, message, Snackbar.LENGTH_SHORT).show()
    }
}
