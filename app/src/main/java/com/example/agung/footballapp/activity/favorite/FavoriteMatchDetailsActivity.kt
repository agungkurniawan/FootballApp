package com.example.agung.footballapp.activity.favorite

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.agung.footballapp.R
import com.example.agung.footballapp.changeDateFormat
import com.example.agung.footballapp.changeTime
import com.example.agung.footballapp.model.FavoriteMatch
import com.example.agung.footballapp.presenter.favorite.FavoriteMatchDetailsPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_favorite_match_details.*

class FavoriteMatchDetailsActivity : AppCompatActivity() {
    private lateinit var matchInfo: FavoriteMatch
    private lateinit var presenter: FavoriteMatchDetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_match_details)

        if (intent?.extras?.containsKey("data_favorite")!!) {
            matchInfo = intent.getParcelableExtra("data_favorite")

            supportActionBar?.title = "${matchInfo.nameHomeTeam} vs ${matchInfo.nameAwayTeam}"

            presenter = FavoriteMatchDetailsPresenter(this)
            displayInfo(matchInfo)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)

        if (presenter.isFavorite(matchInfo.id)) {
            menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_star_black_24dp)
        } else {
            menu?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_star_border_black_24dp)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                onBackPressed()
                true
            }
            R.id.add_to_favorite -> {
                if (presenter.isFavorite(matchInfo.id)) {
                    presenter.removeFromFavorite(matchInfo.id)
                    item.icon = ContextCompat.getDrawable(this, R.drawable.ic_star_border_black_24dp)
                } else {
                    presenter.addToFavorites(matchInfo)
                    item.icon = ContextCompat.getDrawable(this, R.drawable.ic_star_black_24dp)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun displayInfo(items: FavoriteMatch) {
        Picasso.get().load(items.logoHomeTeam).into(iv_home_team)
        Picasso.get().load(items.logoAwayTeam).into(iv_away_team)
        tv_date.text = changeDateFormat(items.matchDate.toString())
        tv_time.text = changeTime(items.matchDate.toString(), items.matchTime.toString())
        if (items.scoreHomeTeam == null && items.scoreAwayTeam == null) {
            tv_score_home.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
            tv_score_away.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        } else {
            tv_score_home.text = items.scoreHomeTeam.toString()
            tv_score_away.text = items.scoreAwayTeam.toString()
        }
        tv_home_team.text = items.nameHomeTeam
        tv_away_team.text = items.nameAwayTeam
        tv_home_goals.text = items.goalsHomeTeam?.replace(";", "\n")
        tv_away_goals.text = items.goalsAwayTeam?.replace(";", "\n")
        tv_home_shots.text = items.shotsHomeTeam.toString()
        tv_away_shots.text = items.shotsAwayTeam.toString()
        tv_home_goalkeeper.text = items.goalkeeperHomeTeam?.replace(";", "")
        tv_away_goalkeeper.text = items.goalkeeperAwayTeam?.replace(";", "")
        tv_home_defense.text = items.defenseHomeTeam?.replace("; ", "\n")
        tv_away_defense.text = items.defenseAwayTeam?.replace("; ", "\n")
        tv_home_midfield.text = items.midfieldHomeTeam?.replace("; ", "\n")
        tv_away_midfield.text = items.midfieldAwayTeam?.replace("; ", "\n")
        tv_home_forward.text = items.forwardHomeTeam?.replace("; ", "\n")
        tv_away_forward.text = items.forwardAwayTeam?.replace("; ", "\n")
    }
}
