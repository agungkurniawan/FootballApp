package com.example.agung.footballapp.view.match

import com.example.agung.footballapp.model.match.EventsItem

interface MatchDetailsView {
    fun displayInfo(items: EventsItem)
    fun displayClubLogo(logoUrl: String?, type: String?)
    fun showSnackbar(message: String)
}