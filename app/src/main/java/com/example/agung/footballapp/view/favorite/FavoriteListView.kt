package com.example.agung.footballapp.view.favorite

import com.example.agung.footballapp.model.FavoriteMatch
import com.example.agung.footballapp.model.FavoriteTeam

interface FavoriteListView {
    fun showDataMatch(items: List<FavoriteMatch>)
    fun showDataTeam(items: List<FavoriteTeam>)
    fun showSnackbar(message: String)
    fun hideLoading()
    fun hideRecyclerView()
}