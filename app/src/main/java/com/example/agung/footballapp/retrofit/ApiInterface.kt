package com.example.agung.footballapp.retrofit

import com.example.agung.footballapp.model.match.Match
import com.example.agung.footballapp.model.matchdetails.Details
import com.example.agung.footballapp.model.player.Player
import com.example.agung.footballapp.model.search.SearchResults
import com.example.agung.footballapp.model.teams.TeamList
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    // For past matches
    @GET("eventspastleague.php")
    fun getLastMatch(@Query("id") idLeague: String): Call<Match>

    // For next matches
    @GET("eventsnextleague.php")
    fun getNextMatch(@Query("id") idLeague: String): Call<Match>

    // Team info
    @GET("lookupteam.php")
    fun getTeamInfo(@Query("id") idTeam: String): Call<Details>

    // Search match
    @GET("searchevents.php")
    fun getSearchResults(@Query("e") keyword: String): Call<SearchResults>

    // Team list by league
    @GET("lookup_all_teams.php")
    fun getTeamByLeague(@Query("id") idLeague: String): Call<TeamList>

    // Get player by club ID
    @GET("lookup_all_players.php")
    fun getPlayerByClub(@Query("id") idClub: String): Call<Player>

    // Search teams
    @GET("searchteams.php")
    fun getTeamBySearch(@Query("t") team: String): Call<TeamList>

    // for instrument test
    @GET("eventspastleague.php")
    fun testLastMatch(@Query("id") idLeague: String): Deferred<Match>
}