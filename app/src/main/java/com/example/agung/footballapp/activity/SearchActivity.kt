package com.example.agung.footballapp.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.Toast
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.match.MatchFragment
import com.example.agung.footballapp.adapter.SearchMatchAdapter
import com.example.agung.footballapp.adapter.SearchTeamAdapter
import com.example.agung.footballapp.model.match.EventsItem
import com.example.agung.footballapp.model.teams.TeamsItem
import com.example.agung.footballapp.presenter.SearchPresenter
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.SearchMatchTeamView
import kotlinx.android.synthetic.main.activity_search_match.*

class SearchActivity : AppCompatActivity(), SearchMatchTeamView {
    private lateinit var presenter: SearchPresenter
    private lateinit var apiClass: ApiClass

    private lateinit var searchView: SearchView

    private var items: MutableList<EventsItem> = mutableListOf()
    private var teamsItem: MutableList<TeamsItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_match)

        apiClass = ApiClass()
        presenter = SearchPresenter(this, apiClass)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)

        val searchIcon = menu?.findItem(R.id.search)
        searchView = searchIcon?.actionView as SearchView
        searchView.requestFocusFromTouch()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0?.length!! > 1) {
                    if (intent?.extras?.containsKey("src_class")!!) {
                        val srcActivity = intent.getStringExtra("src_class")

                        if (srcActivity == MatchFragment::class.java.simpleName) {
                            presenter.getSearchResult(p0)
                        } else {
                            presenter.getTeamSearch(p0)
                        }
                    }
                }

                return false
            }

            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }
        })

        return true
    }

    override fun showLoading() {
        pb_search.visibility = View.VISIBLE
        rv_search.visibility = View.GONE
    }

    override fun hideLoading() {
        pb_search.visibility = View.GONE
        rv_search.visibility = View.VISIBLE
    }

    override fun showData(data: List<EventsItem>) {
        hideLoading()

        Log.d(SearchActivity::class.java.simpleName, "Search results on Data: $data")
        items.clear()

        data.forEach {
            if (it.strSport.equals("Soccer")) {
                items.add(it)
            }
        }

        val adapter = SearchMatchAdapter(this, items)
        rv_search.hasFixedSize()
        rv_search.layoutManager = LinearLayoutManager(this)
        rv_search.adapter = adapter

        Log.d(SearchActivity::class.java.simpleName, "Search results: $items")

        adapter.notifyDataSetChanged()
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showTeam(data: List<TeamsItem>) {
        hideLoading()
        Log.d(SearchActivity::class.java.simpleName, "Search results on Team Data: $data")

        teamsItem.clear()
        data.forEach {
            if (it.strSport.equals("Soccer")) {
                teamsItem.add(it)
            }
        }

        val adapter = SearchTeamAdapter(this, data)
        rv_search.hasFixedSize()
        rv_search.layoutManager = LinearLayoutManager(this)
        rv_search.adapter = adapter

        Log.d(SearchActivity::class.java.simpleName, "Search team results: $teamsItem")

        adapter.notifyDataSetChanged()
    }
}
