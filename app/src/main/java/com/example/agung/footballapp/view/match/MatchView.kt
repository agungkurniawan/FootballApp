package com.example.agung.footballapp.view.match

import com.example.agung.footballapp.model.match.EventsItem

interface MatchView {
    fun showData(data: List<EventsItem>)
    fun showSpinner()
}