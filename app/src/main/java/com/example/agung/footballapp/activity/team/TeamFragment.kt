package com.example.agung.footballapp.activity.team

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.SearchActivity
import com.example.agung.footballapp.adapter.TeamAdapter
import com.example.agung.footballapp.model.teams.TeamsItem
import com.example.agung.footballapp.presenter.team.TeamPresenter
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.team.TeamView
import kotlinx.android.synthetic.main.fragment_team.*
import kotlinx.android.synthetic.main.fragment_team.view.*

class TeamFragment : Fragment(), TeamView {
    private lateinit var presenter: TeamPresenter
    private lateinit var apiClass: ApiClass

    private var rootView: View? = null

    private var leagueId = "4328"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        apiClass = ApiClass()
        presenter = TeamPresenter(this, apiClass)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_team, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.inflateMenu(R.menu.menu_main)

        toolbar.setOnMenuItemClickListener(Toolbar.OnMenuItemClickListener {
            return@OnMenuItemClickListener when (it.itemId) {
                R.id.search -> {
                    Log.d(TeamFragment::class.java.simpleName, "Search clicked")
                    val i = Intent(context, SearchActivity::class.java)
                    i.putExtra("src_class", TeamFragment::class.java.simpleName)
                    startActivity(i)
                    true
                }
                else -> true
            }
        })

        showSpinner()

        swipe_teams.isEnabled = true
        swipe_teams.isRefreshing = true
        swipe_teams.setColorSchemeColors(ContextCompat.getColor(context!!, R.color.colorPrimary))
        swipe_teams.setOnRefreshListener {
            presenter.getTeamList(leagueId)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getTeamList(leagueId)
    }

    override fun showSpinner() {
        sp_league.visibility = View.VISIBLE
        val idLeagueArray = resources.getStringArray(R.array.league_id)
        val leagueName = resources.getStringArray(R.array.league_name)
        val adapter = ArrayAdapter<String>(
            this.activity as Context,
            android.R.layout.simple_spinner_item,
            leagueName
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_league.adapter = adapter

        sp_league.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (leagueId != idLeagueArray[position]) {
                    leagueId = idLeagueArray[position]
                    swipe_teams.isRefreshing = true
                    presenter.getTeamList(leagueId)
                }
            }
        }
    }

    override fun showData(data: List<TeamsItem>) {
        Log.d(TeamFragment::class.java.simpleName, data.toString())

        val adapter = TeamAdapter(context, data)

        view?.rootView?.rv_teams?.hasFixedSize()
        view?.rootView?.rv_teams?.layoutManager = LinearLayoutManager(context)
        view?.rootView?.rv_teams?.adapter = adapter

        view?.rootView?.swipe_teams?.isRefreshing = false
        adapter.notifyDataSetChanged()
    }

    override fun showSnackbar(message: String) {
        Snackbar.make(rv_teams, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun showSearchResult(data: List<TeamsItem>) {
        sp_league.visibility = View.GONE
        swipe_teams.isEnabled = false

        val adapter = TeamAdapter(activity as Activity, data)
        rv_teams.hasFixedSize()
        rv_teams.layoutManager = LinearLayoutManager(context)
        rv_teams.adapter = adapter

        swipe_teams.isRefreshing = false
        adapter.notifyDataSetChanged()
    }
}
