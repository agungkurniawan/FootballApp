package com.example.agung.footballapp.presenter.favorite

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.agung.footballapp.database.database
import com.example.agung.footballapp.model.FavoriteTeam
import com.example.agung.footballapp.view.team.TeamDetailsView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class FavoriteTeamDetailsPresenter(private val context: Context?, private val view: TeamDetailsView) {
    fun addToFavorite(teamInfo: FavoriteTeam) {
        try {
            context?.database?.use {
                insert(
                    FavoriteTeam.TABLE_FAVORITE,
                    FavoriteTeam.ID to teamInfo.id,
                    FavoriteTeam.TEAM_NAME to teamInfo.teamName,
                    FavoriteTeam.FOUNDED_YEAR to teamInfo.foundedYear,
                    FavoriteTeam.STADION to teamInfo.stadion,
                    FavoriteTeam.DESCRIPTION to teamInfo.description,
                    FavoriteTeam.TEAM_LOGO to teamInfo.teamLogo)
            }
            view.showSnackbar("Saved to favorite")
        } catch (e: SQLiteConstraintException) {
            Log.e(FavoriteTeamDetailsPresenter::class.java.simpleName, e.localizedMessage)
        }
    }

    fun isFavorite(idClub: String): Boolean {
        var isFav = false
        context?.database?.use {
            val result = select(FavoriteTeam.TABLE_FAVORITE).whereArgs("${FavoriteTeam.ID} = {id}", "id" to idClub).parseList(
                classParser<FavoriteTeam>()
            )
            isFav = result.isNotEmpty()
        }

        return isFav
    }

    fun removeFromFavorite(idClub: String) {
        try {
            context?.database?.use {
                delete(
                    FavoriteTeam.TABLE_FAVORITE,
                    "${FavoriteTeam.ID} = {id}",
                    "id" to idClub)
            }

            view.showSnackbar("Removed from favorite")
        } catch (e: SQLiteConstraintException) {
            Log.e(FavoriteTeamDetailsPresenter::class.java.simpleName, e.localizedMessage)
        }
    }
}