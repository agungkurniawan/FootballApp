package com.example.agung.footballapp.activity.favorite

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import kotlinx.android.synthetic.main.fragment_favorite.view.*

class FavoriteFragment : Fragment() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_favorite, container, false)

        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        v.vp_container.adapter = mSectionsPagerAdapter

        v.vp_container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(v.tabs))
        v.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(v.vp_container))

        return v
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(p0: Int): Fragment {
            return FavoriteListFragment.newInstance(p0)
        }

        override fun getCount(): Int {
            return 2
        }
    }

}
