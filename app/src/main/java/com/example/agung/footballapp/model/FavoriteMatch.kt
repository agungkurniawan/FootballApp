package com.example.agung.footballapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FavoriteMatch(val id: Int? = null,
                         val matchDate: String? = null,
                         val matchTime: String? = null,
                         val nameHomeTeam: String? = null,
                         val nameAwayTeam: String? = null,
                         val logoHomeTeam: String? = null,
                         val logoAwayTeam: String? = null,
                         val scoreHomeTeam: Int? = null,
                         val scoreAwayTeam: Int? = null,
                         val goalsHomeTeam: String? = null,
                         val goalsAwayTeam: String? = null,
                         val shotsHomeTeam: Int? = null,
                         val shotsAwayTeam: Int? = null,
                         val goalkeeperHomeTeam: String? = null,
                         val goalkeeperAwayTeam: String? = null,
                         val defenseHomeTeam: String? = null,
                         val defenseAwayTeam: String? = null,
                         val midfieldHomeTeam: String? = null,
                         val midfieldAwayTeam: String? = null,
                         val forwardHomeTeam: String? = null,
                         val forwardAwayTeam: String? = null): Parcelable {
    companion object {
        const val TABLE_FAVORITE = "FAVORITE_MATCH"
        const val ID = "ID_MATCH"
        const val MATCH_DATE = "DATE"
        const val MATCH_TIME = "TIME"
        const val NAME_HOME = "NAME_HOME_TEAM"
        const val NAME_AWAY = "NAME_AWAY_TEAM"
        const val LOGO_HOME = "LOGO_HOME_URL"
        const val LOGO_AWAY = "LOGO_AWAY_URL"
        const val SCORE_HOME = "SCORE_HOME_TEAM"
        const val SCORE_AWAY = "SCORE_AWAY_TEAM"
        const val GOALS_HOME = "GOALS_HOME_TEAM"
        const val GOALS_AWAY = "GOALS_AWAY_TEAM"
        const val SHOTS_HOME = "SHOTS_HOME_TEAM"
        const val SHOTS_AWAY = "SHOTS_AWAY_TEAM"
        const val GOALKEEPER_HOME = "GOALKEEPER_HOME_TEAM"
        const val GOALKEEPER_AWAY = "GOALKEEPER_AWAY_TEAM"
        const val DEFENSE_HOME = "DEFENSE_HOME_TEAM"
        const val DEFENSE_AWAY = "DEFENSE_AWAY_TEAM"
        const val MIDFIELD_HOME = "MIDFIELD_HOME_TEAM"
        const val MIDFIELD_AWAY = "MIDFIELD_AWAY_TEAM"
        const val FORWARD_HOME = "FORWARD_HOME_TEAM"
        const val FORWARD_AWAY = "FORWARD_AWAY_TEAM"
    }
}