package com.example.agung.footballapp.adapter

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.match.MatchDetailsActivity
import com.example.agung.footballapp.changeDateFormat
import com.example.agung.footballapp.changeTime
import com.example.agung.footballapp.model.match.EventsItem
import kotlinx.android.synthetic.main.list_main.view.*

class SearchMatchAdapter(private val context: Context, private val list: List<EventsItem>) :
    RecyclerView.Adapter<SearchMatchAdapter.SearchMatchViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SearchMatchViewHolder {
        return SearchMatchViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.list_main,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(p0: SearchMatchViewHolder, p1: Int) {
        p0.bindItem(list[p1])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class SearchMatchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(match: EventsItem) {
            itemView.tv_date.text = changeDateFormat(match.dateEvent.toString())
            itemView.tv_time.text = changeTime(match.dateEvent.toString(), match.strTime.toString())

            if (match.strHomeTeam!!.length >= 10)
                itemView.tv_home_team.text = match.strHomeTeam.substring(0..8) + "..."
            else
                itemView.tv_home_team.text = match.strHomeTeam

            if (match.strAwayTeam!!.length >= 10)
                itemView.tv_away_team.text = match.strAwayTeam.substring(0..8) + "..."
            else
                itemView.tv_away_team.text = match.strAwayTeam

            if (match.intHomeScore != null && match.intAwayScore != null) {
                itemView.tv_score_home.text = match.intHomeScore.toString()
                itemView.tv_score_away.text = match.intAwayScore.toString()
            } else {
                itemView.tv_score_home.text = "-"
                itemView.tv_score_away.text = "-"

                itemView.tv_score_home.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimary
                    )
                )
                itemView.tv_score_away.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimary
                    )
                )
            }

            itemView.setOnClickListener {
                val i = Intent(context, MatchDetailsActivity::class.java)
                i.putExtra("data", match as Parcelable)
                context.startActivity(i)
            }
        }
    }
}