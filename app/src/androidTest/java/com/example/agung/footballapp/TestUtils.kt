package com.example.agung.footballapp

import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher

fun <T> result(matcher: Matcher<T>, i: Int): Matcher<T> {
    return object : BaseMatcher<T>() {
        private var resultIndex = -1
        override fun matches(item: Any): Boolean {
            if (matcher.matches(item)) {
                resultIndex++
                return resultIndex == i
            }
            return false
        }

        override fun describeTo(description: Description) {}
    }
}