package com.example.agung.footballapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.team.PlayerDetailsActivity
import com.example.agung.footballapp.model.player.PlayerItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_player.view.*

class PlayerAdapter(private val context: Context, private val items: List<PlayerItem>)
    : RecyclerView.Adapter<PlayerAdapter.PlayerViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PlayerViewHolder {
        return PlayerViewHolder(LayoutInflater.from(context).inflate(R.layout.list_player, p0, false))
    }

    override fun onBindViewHolder(p0: PlayerViewHolder, p1: Int) {
        p0.bindItem(items[p1])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class PlayerViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bindItem(item: PlayerItem) {
            Picasso.get().load(item.strCutout).into(itemView.iv_player)
            itemView.tv_player_name.text = item.strPlayer
            itemView.tv_player_position.text = item.strPosition

            itemView.setOnClickListener {
                val i = Intent(context, PlayerDetailsActivity::class.java)
                i.putExtra("player_info", item)
                context.startActivity(i)
            }
        }
    }
}