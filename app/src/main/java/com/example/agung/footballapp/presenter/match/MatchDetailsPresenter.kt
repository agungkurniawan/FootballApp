package com.example.agung.footballapp.presenter.match

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.agung.footballapp.database.database
import com.example.agung.footballapp.model.FavoriteMatch
import com.example.agung.footballapp.model.match.EventsItem
import com.example.agung.footballapp.model.matchdetails.Details
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.match.MatchDetailsView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MatchDetailsPresenter(private val view: MatchDetailsView, private val context: Context?) {
    private val apiClass = ApiClass()

    // Fetch the logo URL from API
    fun getLogo(idTeam: String?, teamType: String): String? {
        val getLogoApi = apiClass.getApiInterface()?.getTeamInfo(idTeam.toString())
        var logoUrl: String? = null

        getLogoApi?.enqueue(object : Callback<Details> {
            override fun onFailure(call: Call<Details>, t: Throwable) {
                view.showSnackbar("Failed to get club logo(s)")
            }

            override fun onResponse(call: Call<Details>, response: Response<Details>) {
                if (response.isSuccessful) {
                    val details = response.body()
                    val imageInfo = details?.teams

                    logoUrl = imageInfo?.get(0)?.strTeamBadge.toString()

                    view.displayClubLogo(logoUrl, teamType)
                }
            }
        })

        return logoUrl
    }

    fun addToFavorite(matchInfo: EventsItem, homeLogo: String, awayLogo: String) {
        try {
            context?.database?.use {
                insert(
                    FavoriteMatch.TABLE_FAVORITE,
                    FavoriteMatch.ID to matchInfo.idEvent,
                    FavoriteMatch.MATCH_DATE to matchInfo.dateEvent,
                    FavoriteMatch.MATCH_TIME to matchInfo.strTime,
                    FavoriteMatch.NAME_HOME to matchInfo.strHomeTeam,
                    FavoriteMatch.NAME_AWAY to matchInfo.strAwayTeam,
                    FavoriteMatch.LOGO_HOME to homeLogo,
                    FavoriteMatch.LOGO_AWAY to awayLogo,
                    FavoriteMatch.SCORE_HOME to matchInfo.intHomeScore,
                    FavoriteMatch.SCORE_AWAY to matchInfo.intAwayScore,
                    FavoriteMatch.GOALS_HOME to matchInfo.strHomeGoalDetails,
                    FavoriteMatch.GOALS_AWAY to matchInfo.strAwayGoalDetails,
                    FavoriteMatch.SHOTS_HOME to matchInfo.intHomeShots,
                    FavoriteMatch.SHOTS_AWAY to matchInfo.intAwayShots,
                    FavoriteMatch.GOALKEEPER_HOME to matchInfo.strHomeLineupGoalkeeper,
                    FavoriteMatch.GOALKEEPER_AWAY to matchInfo.strAwayLineupGoalkeeper,
                    FavoriteMatch.DEFENSE_HOME to matchInfo.strHomeLineupDefense,
                    FavoriteMatch.DEFENSE_AWAY to matchInfo.strAwayLineupDefense,
                    FavoriteMatch.MIDFIELD_HOME to matchInfo.strHomeLineupMidfield,
                    FavoriteMatch.MIDFIELD_AWAY to matchInfo.strAwayLineupMidfield,
                    FavoriteMatch.FORWARD_HOME to matchInfo.strHomeLineupForward,
                    FavoriteMatch.FORWARD_AWAY to matchInfo.strAwayLineupForward
                )
            }
            view.showSnackbar("Added to favorite")
        } catch (e: SQLiteConstraintException) {
            Log.e(MatchDetailsPresenter::class.java.simpleName, e.localizedMessage)
        }
    }

    fun isFavorite(idMatch: String?): Boolean {
        var isNotEmpty = false

        context?.database?.use {
            val result = select(FavoriteMatch.TABLE_FAVORITE)
                .whereArgs("${FavoriteMatch.ID} = {id}",
                    "id" to idMatch.toString())
                .parseList(classParser<FavoriteMatch>())
            isNotEmpty = result.isNotEmpty()
        }

        return isNotEmpty
    }

    fun removeFromFavorite(idMatch: String?) {
        try {
            context?.database?.use {
                delete(FavoriteMatch.TABLE_FAVORITE,
                    "${FavoriteMatch.ID} = {id}",
                    "id" to idMatch.toString())
            }

            view.showSnackbar("Removed from favorite")
        } catch (e: SQLiteConstraintException) {
            Log.e(MatchDetailsPresenter::class.java.simpleName, e.localizedMessage)
        }
    }
}