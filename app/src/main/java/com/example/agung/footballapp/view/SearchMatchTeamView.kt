package com.example.agung.footballapp.view

import com.example.agung.footballapp.model.match.EventsItem
import com.example.agung.footballapp.model.teams.TeamsItem

interface SearchMatchTeamView {
    fun showData(data: List<EventsItem>)
    fun showLoading()
    fun hideLoading()
    fun showToast(message: String)
    fun showTeam(data: List<TeamsItem>)
}