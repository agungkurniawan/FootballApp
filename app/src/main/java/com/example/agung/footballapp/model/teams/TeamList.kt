package com.example.agung.footballapp.model.teams

import com.google.gson.annotations.SerializedName

data class TeamList(

    @field:SerializedName("teams")
    val teams: List<TeamsItem?>? = null
)