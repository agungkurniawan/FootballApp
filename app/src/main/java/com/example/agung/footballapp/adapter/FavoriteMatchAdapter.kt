package com.example.agung.footballapp.adapter

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.favorite.FavoriteMatchDetailsActivity
import com.example.agung.footballapp.changeDateFormat
import com.example.agung.footballapp.changeTime
import com.example.agung.footballapp.model.FavoriteMatch
import kotlinx.android.synthetic.main.list_main.view.*

class FavoriteMatchAdapter(private val context: Context?, private val list: List<FavoriteMatch>)
    : RecyclerView.Adapter<FavoriteMatchAdapter.FavoriteViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FavoriteViewHolder {
        return FavoriteViewHolder(LayoutInflater.from(context).inflate(R.layout.list_main, p0, false))
    }

    override fun onBindViewHolder(p0: FavoriteViewHolder, p1: Int) {
        p0.bindItem(list[p1])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class FavoriteViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bindItem(match: FavoriteMatch) {
            itemView.tv_date.text = changeDateFormat(match.matchDate.toString())
            itemView.tv_time.text = changeTime(match.matchDate.toString(), match.matchTime.toString())

            if (match.nameHomeTeam!!.length >= 10)
                itemView.tv_home_team.text = match.nameHomeTeam.substring(0..8) + "..."
            else
                itemView.tv_home_team.text = match.nameHomeTeam

            if (match.nameAwayTeam!!.length >= 10)
                itemView.tv_away_team.text = match.nameAwayTeam.substring(0..8) + "..."
            else
                itemView.tv_away_team.text = match.nameAwayTeam

            if (match.scoreHomeTeam != null && match.scoreAwayTeam != null) {
                itemView.tv_score_home.text = match.scoreHomeTeam.toString()
                itemView.tv_score_away.text = match.scoreAwayTeam.toString()
            } else {
                itemView.tv_score_home.text = "-"
                itemView.tv_score_away.text = "-"

                itemView.tv_score_home.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.colorPrimary
                    )
                )
                itemView.tv_score_away.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimary
                    )
                )
            }

            itemView.setOnClickListener {
                val intent = Intent(context, FavoriteMatchDetailsActivity::class.java)
                intent.putExtra("data_favorite", match as Parcelable)
                context?.startActivity(intent)
            }
        }
    }
}