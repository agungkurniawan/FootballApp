package com.example.agung.footballapp.model.search

import com.example.agung.footballapp.model.match.EventsItem
import com.google.gson.annotations.SerializedName

data class SearchResults(

    @field:SerializedName("event")
    val event: List<EventsItem?>? = null
)