package com.example.agung.footballapp

import java.text.SimpleDateFormat
import java.util.*

fun changeDateFormat(srcDate: String): String {
    val oldFormat = SimpleDateFormat("yyyy-MM-dd")
    val newFormat = SimpleDateFormat("EE, d MMM yyyy")

    val parseDate = oldFormat.parse(srcDate)

    return newFormat.format(parseDate)
}

fun changeTime(srcDate: String, srcTime: String): String {
    val formatToTimestamp = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    formatToTimestamp.timeZone = TimeZone.getTimeZone("UTC")
    val dateTime = formatToTimestamp.parse("$srcDate $srcTime")

    val formatToHourMinute = SimpleDateFormat("HH:mm")
    return formatToHourMinute.format(dateTime)
}