package com.example.agung.footballapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.team.TeamDetailsActivity
import com.example.agung.footballapp.model.teams.TeamsItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_team.view.*

class TeamAdapter(private val context: Context?, private val items: List<TeamsItem>) :
    RecyclerView.Adapter<TeamAdapter.TeamViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TeamViewHolder {
        return TeamViewHolder(LayoutInflater.from(context).inflate(R.layout.list_team, p0, false))
    }

    override fun onBindViewHolder(p0: TeamViewHolder, p1: Int) {
        p0.bindItem(items[p1])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class TeamViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bindItem(items: TeamsItem) {
            Picasso.get().load(items.strTeamBadge).into(itemView.iv_logo_team)
            itemView.tv_team_name.text = items.strTeam

            itemView.setOnClickListener {
                val i = Intent(context, TeamDetailsActivity::class.java)
                i.putExtra("team_data", items)
                context?.startActivity(i)
            }
        }
    }
}