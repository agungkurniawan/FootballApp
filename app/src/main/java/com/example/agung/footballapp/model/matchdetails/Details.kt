package com.example.agung.footballapp.model.matchdetails

import com.google.gson.annotations.SerializedName

data class Details(
    @field:SerializedName("teams")
    val teams: List<TeamsItem?>? = null
)