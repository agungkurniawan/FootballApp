package com.example.agung.footballapp.retrofit

import com.example.agung.footballapp.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClass {
    private var apiInterface: ApiInterface? = null

    fun getApiInterface(): ApiInterface? {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        if (apiInterface == null) {
            val gson: Gson = GsonBuilder().serializeNulls().create()
            val retrofit: Retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

            apiInterface = retrofit.create(ApiInterface::class.java)
        }

        return apiInterface
    }
}