package com.example.agung.footballapp.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.agung.footballapp.model.FavoriteMatch
import com.example.agung.footballapp.model.FavoriteTeam
import org.jetbrains.anko.db.*

class MainDatabaseOpenHelper(context: Context) :
    ManagedSQLiteOpenHelper(context, "FavoriteTeam.db", null, 1) {
    companion object {
        private var instance: MainDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(context: Context): MainDatabaseOpenHelper {
            if (instance == null) {
                instance = MainDatabaseOpenHelper(context.applicationContext)
            }

            return instance as MainDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        // Create table
        db?.createTable(
            FavoriteMatch.TABLE_FAVORITE, true,
            FavoriteMatch.ID to INTEGER + PRIMARY_KEY,
            FavoriteMatch.MATCH_DATE to TEXT,
            FavoriteMatch.MATCH_TIME to TEXT,
            FavoriteMatch.NAME_HOME to TEXT,
            FavoriteMatch.NAME_AWAY to TEXT,
            FavoriteMatch.LOGO_HOME to TEXT,
            FavoriteMatch.LOGO_AWAY to TEXT,
            FavoriteMatch.SCORE_HOME to INTEGER,
            FavoriteMatch.SCORE_AWAY to INTEGER,
            FavoriteMatch.GOALS_HOME to TEXT,
            FavoriteMatch.GOALS_AWAY to TEXT,
            FavoriteMatch.SHOTS_HOME to INTEGER,
            FavoriteMatch.SHOTS_AWAY to INTEGER,
            FavoriteMatch.GOALKEEPER_HOME to TEXT,
            FavoriteMatch.GOALKEEPER_AWAY to TEXT,
            FavoriteMatch.DEFENSE_HOME to TEXT,
            FavoriteMatch.DEFENSE_AWAY to TEXT,
            FavoriteMatch.MIDFIELD_HOME to TEXT,
            FavoriteMatch.MIDFIELD_AWAY to TEXT,
            FavoriteMatch.FORWARD_HOME to TEXT,
            FavoriteMatch.FORWARD_AWAY to TEXT
        )

        db?.createTable(
            FavoriteTeam.TABLE_FAVORITE, true,
            FavoriteTeam.ID to INTEGER + PRIMARY_KEY,
            FavoriteTeam.TEAM_NAME to TEXT,
            FavoriteTeam.FOUNDED_YEAR to INTEGER,
            FavoriteTeam.STADION to TEXT,
            FavoriteTeam.DESCRIPTION to TEXT,
            FavoriteTeam.TEAM_LOGO to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // Upgrade tables
        db?.dropTable(FavoriteMatch.TABLE_FAVORITE, true)
        db?.dropTable(FavoriteTeam.TABLE_FAVORITE, true)
    }
}

// Access property for Context
val Context.database: MainDatabaseOpenHelper
    get() = MainDatabaseOpenHelper.getInstance(applicationContext)