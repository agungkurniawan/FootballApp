package com.example.agung.footballapp.view.team

import com.example.agung.footballapp.model.teams.TeamsItem

interface TeamView {
    fun showData(data: List<TeamsItem>)
    fun showSpinner()
    fun showSnackbar(message: String)
    fun showSearchResult(data: List<TeamsItem>)
}