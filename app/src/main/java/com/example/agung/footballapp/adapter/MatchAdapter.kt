package com.example.agung.footballapp.adapter

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.provider.CalendarContract
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.match.MatchDetailsActivity
import com.example.agung.footballapp.changeDateFormat
import com.example.agung.footballapp.changeTime
import com.example.agung.footballapp.model.match.EventsItem
import kotlinx.android.synthetic.main.list_main.view.*
import java.util.*

class MatchAdapter(private val context: Context?, private val match: List<EventsItem>) :
        RecyclerView.Adapter<MatchAdapter.MainViewHolder>() {
    var isNextMatch = false

    constructor(context: Context?, match: List<EventsItem>, isNextMatch: Boolean): this(context, match) {
        this.isNextMatch = isNextMatch
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MainViewHolder {
        return MainViewHolder(LayoutInflater.from(context).inflate(R.layout.list_main, p0, false))
    }

    override fun onBindViewHolder(p0: MainViewHolder, p1: Int) {
        p0.bindItem(match[p1])
    }

    override fun getItemCount(): Int {
        return match.size
    }

    inner class MainViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(match: EventsItem) {
            if (isNextMatch) {
                itemView.iv_add_event.visibility = View.VISIBLE
                itemView.iv_add_event.setOnClickListener {
                    val addToCal = Intent(Intent.ACTION_INSERT)
                    addToCal.type = "vnd.android.cursor.item/event"
                    addToCal.putExtra(CalendarContract.Events.TITLE, match.strFilename)

                    val timeNew = changeTime(match.dateEvent.toString(), match.strTime.toString())

                    val year = match.dateEvent?.substring(0..3)?.toInt()!!
                    val month = match.dateEvent.substring(5..6).toInt()
                    val date = match.dateEvent.substring(8..9).toInt()
                    val hour = timeNew.substring(0..1).toInt()
                    val minute = timeNew.substring(3..4).toInt()

                    Log.d(MatchAdapter::class.java.simpleName, "YearEvent: $year/$month/$date")

                    val dateEvent = GregorianCalendar(year, month-1, date, hour, minute)
                    addToCal.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
                    addToCal.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, dateEvent.timeInMillis)
                    addToCal.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, dateEvent.timeInMillis)

                    context?.startActivity(addToCal)
                }
            }

            itemView.tv_date.text = changeDateFormat(match.dateEvent.toString())
            itemView.tv_time.text = changeTime(match.dateEvent.toString(), match.strTime.toString())

            if (match.strHomeTeam!!.length >= 10)
                itemView.tv_home_team.text = match.strHomeTeam.substring(0..8) + "..."
            else
                itemView.tv_home_team.text = match.strHomeTeam

            if (match.strAwayTeam!!.length >= 10)
                itemView.tv_away_team.text = match.strAwayTeam.substring(0..8) + "..."
            else
                itemView.tv_away_team.text = match.strAwayTeam

            if (match.intHomeScore != null && match.intAwayScore != null) {
                itemView.tv_score_home.text = match.intHomeScore.toString()
                itemView.tv_score_away.text = match.intAwayScore.toString()
            } else {
                itemView.tv_score_home.text = "-"
                itemView.tv_score_away.text = "-"

                itemView.tv_score_home.setTextColor(
                        ContextCompat.getColor(
                                context!!,
                                R.color.colorPrimary
                        )
                )
                itemView.tv_score_away.setTextColor(
                        ContextCompat.getColor(
                                context,
                                R.color.colorPrimary
                        )
                )
            }

            itemView.setOnClickListener {
                val intent = Intent(context, MatchDetailsActivity::class.java)
                intent.putExtra("data", match as Parcelable)
                context?.startActivity(intent)
            }
        }
    }
}