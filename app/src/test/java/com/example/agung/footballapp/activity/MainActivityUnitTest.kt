package com.example.agung.footballapp.activity

import com.example.agung.footballapp.loadJson
import com.example.agung.footballapp.model.match.EventsItem
import com.example.agung.footballapp.retrofit.ApiInterface
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class MainActivityUnitTest {
    private val okHttpClient = OkHttpClient()
    private val webServer = MockWebServer()

    private lateinit var service: ApiInterface

    @Before
    fun setUp() {
        val retrofit = Retrofit.Builder()
            .baseUrl(webServer.url(""))
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create()))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

        service = retrofit.create(ApiInterface::class.java)
    }

    @Test
    fun returnMatchOn200HttpResponse() {
        return runBlocking {
            val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(loadJson(this.javaClass, "json/response.json"))

            webServer.enqueue(mockResponse)

            val expectedMatch = listOf(
                EventsItem(
                    intHomeShots = "4",
                    strSport = "Soccer",
                    strHomeLineupDefense = "Cesar Azpilicueta; Antonio Ruediger; David Luiz; Marcos Alonso; ",
                    strHomeLineupSubstitutes = "Wilfredo Caballero; Andreas Christensen; Davide Zappacosta; Cesc Fabregas; Ross Barkley; Pedro Rodriguez; Olivier Giroud; ",
                    idLeague = "4328",
                    idSoccerXML = "389973",
                    strHomeLineupForward = "Willian; Alvaro Morata; Eden Hazard; ",
                    strTVStation = null,
                    strHomeGoalDetails = "",
                    strAwayLineupGoalkeeper = "Jordan Pickford; ",
                    strAwayLineupMidfield = "Andre Gomes; Idrissa Gana Gueye; Theo Walcott; Gylfi Sigurdsson; Bernard; ",
                    idEvent = "576588",
                    intRound = "12",
                    strHomeYellowCards = "29':Jorginho;33':N'Golo Kante;45':Antonio Ruediger;88':Alvaro Morata;",
                    idHomeTeam = "133610",
                    intHomeScore = "0",
                    dateEvent = "2018-11-11",
                    strCountry = null,
                    strAwayTeam = "Everton",
                    strHomeLineupMidfield = "N'Golo Kante; Jorginho; Ross Barkley; ",
                    strDate = "11/11/18",
                    strHomeFormation = "",
                    strMap = null,
                    idAwayTeam = "133615",
                    strAwayRedCards = "",
                    strBanner = null,
                    strFanart = null,
                    strDescriptionEN = null,
                    strResult = null,
                    strCircuit = null,
                    intAwayShots = "1",
                    strFilename = "English Premier League 2018-11-10 Chelsea vs Everton",
                    strTime = "14:15:00+00:00",
                    strAwayGoalDetails = "",
                    strAwayLineupForward = "Richarlison; ",
                    strLocked = "unlocked",
                    strSeason = "1819",
                    intSpectators = null,
                    strHomeRedCards = "",
                    strHomeLineupGoalkeeper = "Kepa Arrizabalaga; ",
                    strAwayLineupSubstitutes = "Maarten Stekelenburg; Leighton Baines; Phil Jagielka; Tom Davies; Ademola Lookman; Dominic Calvert-Lewin; Cenk Tosun; ",
                    strAwayFormation = "",
                    strEvent = "Chelsea vs Everton",
                    strAwayYellowCards = "22':Yerry Mina;45':Bernard;73':Jordan Pickford;",
                    strAwayLineupDefense = "Seamus Coleman; Michael Keane; Yerry Mina; Lucas Digne; ",
                    strHomeTeam = "Chelsea",
                    strThumb = null,
                    strLeague = "English Premier League",
                    intAwayScore = "0",
                    strCity = null,
                    strPoster = null
                )
            )

            val actualMatch = service.testLastMatch("4328").await().events
            assertEquals(expectedMatch, actualMatch)
        }
    }

    @Test(expected = HttpException::class)
    fun throwHttpExceptionWhenNon200HttpResponse() {
        return runBlocking {
            val mockResponse = MockResponse().setResponseCode(403)
            webServer.enqueue(mockResponse)

            service.testLastMatch("4328").await()

            Unit
        }
    }
}