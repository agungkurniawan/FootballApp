package com.example.agung.footballapp.presenter.favorite

import android.content.Context
import com.example.agung.footballapp.database.database
import com.example.agung.footballapp.model.FavoriteMatch
import com.example.agung.footballapp.model.FavoriteTeam
import com.example.agung.footballapp.view.favorite.FavoriteListView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavoriteListPresenter(private val context: Context?, private val view: FavoriteListView) {
    fun getMatchList(position: Int) {
        context?.database?.use {
            when (position) {
                0 -> {
                    val favoriteMatch =
                        select(FavoriteMatch.TABLE_FAVORITE).parseList(classParser<FavoriteMatch>())
                    if (favoriteMatch.isNotEmpty()) {
                        view.showDataMatch(favoriteMatch)
                    } else {
                        view.hideLoading()
                        view.hideRecyclerView()
                        view.showSnackbar("You haven't saved anything to favorite")
                    }
                }
                1 -> {
                    val favoriteTeam =
                        select(FavoriteTeam.TABLE_FAVORITE).parseList(classParser<FavoriteTeam>())

                    if (favoriteTeam.isNotEmpty()) {
                        view.showDataTeam(favoriteTeam)
                    } else {
                        view.hideLoading()
                        view.hideRecyclerView()
                        view.showSnackbar("You haven't saved anything to favorite")
                    }
                }
            }
        }
    }
}