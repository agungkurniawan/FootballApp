package com.example.agung.footballapp.activity.match

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.agung.footballapp.R
import com.example.agung.footballapp.activity.SearchActivity
import com.example.agung.footballapp.adapter.MatchAdapter
import com.example.agung.footballapp.model.match.EventsItem
import com.example.agung.footballapp.presenter.match.MatchPresenter
import com.example.agung.footballapp.retrofit.ApiClass
import com.example.agung.footballapp.view.match.MatchView
import kotlinx.android.synthetic.main.fragment_list_match.*
import kotlinx.android.synthetic.main.fragment_list_match.view.*
import kotlinx.android.synthetic.main.fragment_match.*
import kotlinx.android.synthetic.main.fragment_match.view.*

class MatchFragment : Fragment() {
    // This Fragment responsible to show the TabLayout
    // and a Fragment to shows data
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("TEST", MatchFragment::class.java.simpleName)
        val v = inflater.inflate(R.layout.fragment_match, container, false)
        //(activity as AppCompatActivity).setSupportActionBar(toolbar)
        setHasOptionsMenu(true)

        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        v.vp_container.adapter = mSectionsPagerAdapter

        v.vp_container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(v.tabs))
        v.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(v.vp_container))
        setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.inflateMenu(R.menu.menu_main)

        toolbar.setOnMenuItemClickListener(Toolbar.OnMenuItemClickListener {
            return@OnMenuItemClickListener when (it.itemId) {
                R.id.search -> {
                    val i = Intent(context, SearchActivity::class.java)
                    i.putExtra("src_class", MatchFragment::class.java.simpleName)
                    startActivity(i)
                    true
                }
                else -> true
            }
        })
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(p0: Int): Fragment {
            return PlaceholderFragment.newInstance(
                p0
            )
        }

        override fun getCount(): Int {
            return 2
        }
    }

    // Fragment for CardView of Last and Next Matches
    class PlaceholderFragment : Fragment(), MatchView {
        private lateinit var presenter: MatchPresenter
        private lateinit var apiClass: ApiClass

        private var rootView: View? = null

        private var leagueId = "4328"

        companion object {
            private const val ARG_SECTION_NUMBER = "section_number"

            fun newInstance(position: Int): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, position)
                fragment.arguments = args
                return fragment
            }
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            apiClass = ApiClass()
            presenter = MatchPresenter(this, apiClass)
        }

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            rootView = inflater.inflate(R.layout.fragment_list_match, container, false)
            return rootView
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)

            showSpinner()
            swipe_main.isRefreshing = true
            swipe_main.setColorSchemeColors(
                ContextCompat.getColor(
                    context!!,
                    (R.color.colorPrimary)
                )
            )
            swipe_main.setOnRefreshListener {
                presenter.getData(arguments?.getInt(ARG_SECTION_NUMBER), leagueId)
            }

            presenter.getData(arguments?.getInt(ARG_SECTION_NUMBER), leagueId)
        }

        override fun showData(data: List<EventsItem>) {
            Log.d(MatchFragment::class.java.simpleName, data.toString())

            val adapter: MatchAdapter =
                if (arguments?.getInt(ARG_SECTION_NUMBER) == 0) {
                    MatchAdapter(context, data)
                } else {
                    MatchAdapter(context, data, true)
                }

            rootView?.rv_main?.hasFixedSize()
            rootView?.rv_main?.layoutManager = LinearLayoutManager(context)
            rootView?.rv_main?.adapter = adapter

            rootView?.swipe_main?.isRefreshing = false
            adapter.notifyDataSetChanged()
        }

        override fun showSpinner() {
            val idLeagueArray = resources.getStringArray(R.array.league_id)
            val leagueName = resources.getStringArray(R.array.league_name)
            val adapter = ArrayAdapter<String>(
                this.activity as Context,
                android.R.layout.simple_spinner_item,
                leagueName
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_league.adapter = adapter

            sp_league.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (leagueId != idLeagueArray[position]) {
                        leagueId = idLeagueArray[position]
                        swipe_main.isRefreshing = true
                        presenter.getData(arguments?.getInt(ARG_SECTION_NUMBER), leagueId)
                    }
                }
            }
        }
    }
}
