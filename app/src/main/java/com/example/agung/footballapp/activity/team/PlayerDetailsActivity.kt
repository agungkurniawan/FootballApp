package com.example.agung.footballapp.activity.team

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v7.app.AppCompatActivity
import com.example.agung.footballapp.R
import com.example.agung.footballapp.model.player.PlayerItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_player_details.*
import kotlinx.android.synthetic.main.content_player_details.*

class PlayerDetailsActivity : AppCompatActivity() {
    private lateinit var items: PlayerItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent?.extras?.containsKey("player_info")!!) {
            items = intent.getParcelableExtra("player_info")

            toolbarSettings()

            Picasso.get().load(items.strFanart1).into(iv_player_banner)
            showData(items)
        }
    }

    private fun toolbarSettings() {
        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { p0, p1 ->
            var isShow = true
            var scrollRange = -1

            if (scrollRange == -1) {
                scrollRange = p0?.totalScrollRange!!
            }

            if (scrollRange + p1 == 0) {
                toolbar_layout.title = items.strPlayer
                isShow = true
            } else if (isShow) {
                toolbar_layout.title = " "
                isShow = false
            }
        })
    }

    private fun showData(items: PlayerItem) {
        tv_weight.text = items.strWeight
        tv_height.text = items.strHeight
        tv_position.text = items.strPosition
        tv_description.text = items.strDescriptionEN
    }
}
