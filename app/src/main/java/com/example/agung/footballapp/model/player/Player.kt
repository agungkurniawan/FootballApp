package com.example.agung.footballapp.model.player

import com.google.gson.annotations.SerializedName

data class Player(

	@field:SerializedName("player")
	val player: List<PlayerItem?>? = null
)